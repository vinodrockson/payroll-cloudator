﻿
/*
 * GET home page.
 */

exports.index = function (req, res) {
    res.render('register', { message: req.flash('message') });
};

exports.forgotkey = function (req, res) {
    res.render('forgotkey', { message: req.flash('message') });
};

exports.trial = function (req, res) {
    res.render('trial', { message: req.flash('message') });


};

exports.register = function (req, res) {
    res.status(200).render('register', { apikey: req.user.apikey });
}; 

exports.forgot = function (req, res) {
    res.status(200).render('forgotkey', { apikey: req.user.apikey });
};

//Generate work events for input JSON
exports.workevents = function (req, res) {
    var inputData, opFlag = 0, checkInput = 0, trialMsg='';

    if (req.body.jsoninp !== undefined) {
            inputData = JSON.parse(req.body.jsoninp);
            opFlag = 1;
    }
    else
        inputData = req.body;
    console.log(inputData);
    if (inputData.length > 0 && inputData.length !== undefined && inputData.length !== null) {
        var workevents = [], moment = require('moment'), orderBy = require('orderby');
        moment().format();
        
        for (var i = 0; i < inputData.length; i++) {
            var item = inputData[i];
            
            if ((!item.eventsFrom || item.eventsFrom == "") || (!item.eventsTo || item.eventsTo == "") || (!item.employeeName || item.employeeName == "") || (!item.employeeId || item.employeeId == "") || (!item.countryCode || item.countryCode == ""))
                checkInput = checkInput + 1;
            
            if (checkInput === 0) {
                var allHolidays = [], empHolidays = item.holidays, eFromYear = parseInt(item.eventsFrom.split('-')[0]), eToYear = parseInt(item.eventsTo.split('-')[0]), country = item.countryCode;
                for (var j = eFromYear; j <= eToYear; j++) {
                    var temp = getAllBankHolidays(j, country);
                    for (var k = 0; k < temp.length; k++) {
                        var date = temp[k].date.split(' ')[0], type = temp[k].type, name = temp[k].name;
                        if ((type === 'public' || type === 'bank') && moment(date).isBetween(item.eventsFrom, item.eventsTo)) {
                            allHolidays.push({
                                "date" : date,
                                "name" : name
                            });
                        }
                    }
                }
                
                for (var count = 0; count < empHolidays.length; count++) {
                    var isPresentAlready = false;
                    for (var holCount = 0; holCount < allHolidays.length; holCount++) {
                        if (allHolidays[holCount].date === empHolidays[count])
                            isPresentAlready = true;
                    }
                    
                    if (!isPresentAlready)
                        allHolidays.push({
                            "date" : empHolidays[count],
                            "name" : "Employee Leave"
                        });
                }
                
                orderBy(allHolidays, ['date']);
                workevents.push({
                    "employeeName" : item.employeeName,
                    "employeeId"  : item.employeeId,
                    "workEvents"  : allHolidays
                });
            }
        }
        if (checkInput === 0) {
            console.log('opFlag:' + opFlag);
            if (opFlag === 1) {
                var json = JSON.stringify(workevents);
                var filename = 'workevents.json';
                var mimetype = 'application/json';
                res.setHeader('Content-Type', mimetype);
                res.setHeader('Content-disposition', 'attachment; filename=' + filename);
                res.status(200).send(json);
            }
            else
                res.status(200).json(workevents);
        }
        else {
            trialMsg = 'Bad Request - Incorrect JSON Configuration';
            res.status(400).send(trialMsg);
        }
                    
    }
    else {
        trialMsg = 'Bad Request';
        res.status(400).send(trialMsg);
    }
    
    if (opFlag === 1 && trialMsg !== '') {
        req.flash('message', trialMsg);
        res.redirect('/trial');
    }
                   
    req.logout();
};
        
function getAllBankHolidays(year, country) {
    var Holidays = require('date-holidays');
    var hd = new Holidays(country);
    return hd.getHolidays(year);

}
