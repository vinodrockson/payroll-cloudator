﻿
var azure = require("azure-storage");
var q = require('q');
var config = require('./config.js');
var bCrypt = require('bcrypt-nodejs');

function User() { }
    
User.UserFactory = function() {
    var user = {
        companyName : "",
        email : "",
        password : "",
        apikey: "",
        enabled : true,
        verified : false
    };
    user.isValidPassword = User.isValidPassword;
    return user;
}
    

User.findOne = function (email) {
    var tableService = azure.createTableService(config.storageAccountKey);
    var deferred = q.defer();    
    tableService.retrieveEntity('User', email, 'true', function (err, result) {
        if (err) {
            if (err.statusCode === 404)
                deferred.resolve(undefined);
            else
                deferred.reject(err);
        } 
        else {
            deferred.resolve(User.toUser(result));                
        }
    });
    return deferred.promise;
}

// For Authentication by API key
 User.findByApiKey = function (apikey) {
    var tableService = azure.createTableService(config.storageAccountKey);
    var deferred = q.defer();
    console.log(apikey);
    tableService.retrieveEntity('APIKey', apikey, 'true', function (err, result) {
        if (err) {
            if (err.statusCode === 404)
                deferred.resolve(undefined);
            else
                deferred.reject(err);
        } 
        else {
            console.log(result);
            deferred.resolve(result);
        }
    });
    return deferred.promise;
}
/***** Entity Conversions ****/
User.toUser = function (userEntity) {
    var user = User.UserFactory();
    user.email = userEntity.PartitionKey._;
    user.enabled = userEntity.RowKey._;
    user.password = userEntity.password._;
    user.companyName = userEntity.companyname._;
    if(userEntity.apikey._ !== '' && userEntity.apikey._ !== undefined && userEntity.apikey._ !== null)
        user.apikey = userEntity.apikey._;
    return user;
};
    
User.toUserEntity = function (user) {
    return {
        PartitionKey: { '_': user.email },
        RowKey: { '_': 'true' },
        password: { '_': user.password },
        companyname: { '_': user.companyName },
        apikey: { '_': user.apikey}
    };
};

User.toAPIEntity = function (user) {
    return {
        PartitionKey: { '_': user.apikey },
        RowKey: { '_': 'true' },
        email: { '_': user.email }
    };
};
/*****/

// To create user in DB.
User.createUser = function (user) {
    var deferred = q.defer();
    var tableService = azure.createTableService(config.storageAccountKey);
    function create(user) {
        tableService.createTableIfNotExists('User', function (error, result, response) {
            if (!error) {
                tableService.insertEntity('User', User.toUserEntity(user), function (err, result, response) {
                    if (err) {
                        deferred.reject({ status: false, reason: "error creating user", err: err });
                    } 
                    else {
                        tableService.createTableIfNotExists('APIKey', function (error, result, response) {
                            if (!error) {
                                tableService.insertEntity('APIKey', User.toAPIEntity(user), { echoContent: true }, function (errs, result, response) {
                                    if (!errs) {
                                        deferred.resolve(response);
                                    }
                                });
                            }
                        });
                    }
                });
            }
         });
    }

    User.findOne(user.email).then(function (result) {
        if (result && result.length > 1) {
            deferred.resolve({ status: false, reason: "user exists" });
        }
        else {
            create(user);
        }
    }).
    fail(function (err) {
            deferred.reject({ status: false, reason: "error creating user", err: err});
    });

    return deferred.promise;
}
 // Service to update the User data (not implemented in views)   
/* User.save = function (user) {
    var tableService = azure.createTableService(config.storageAccountKey);
    tableService.updateEntity('User', user, function (err, result) {
        if (err) {
            deferred.reject({ status: false, reason: "error creating user" });
        } 
        else {
            deferred.resolve({ status: true, reason: "" });
        }
    });
} */
 
// Create hash for password to store in DB
User.createHash = function (password) {
    return bCrypt.hashSync(password);
}

// Check for hash of password   
User.isValidPassword = function (user, password) {
    return bCrypt.compareSync(password, user.password);
}
    


module.exports = User;
