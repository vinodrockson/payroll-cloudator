﻿
/**
 * Module dependencies.
 */

var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var bCrypt = require('bcrypt-nodejs');
var routes = require('./routes');
var User = require('./userStorage.js');
var http = require('http');
var path = require('path');
var passport = require('passport')
  , flash = require('connect-flash')
  , util = require('util')
  , LocalStrategy1 = require('passport-local').Strategy
  , LocalStrategy2 = require('passport-localapikey').Strategy;
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.methodOverride());
app.use(express.session({ secret: 'p@yroll by vInoD'}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));


//Serialize and De-Serialize User
passport.serializeUser(function (user, done) {
    done(null, user.email);
});

passport.deserializeUser(function (id, done) {
    User.findOne(id).then(function (user) {
        done(null, user);
    }).fail(function (err) {
        done(err);
    });
});

// Register Company User
passport.use('signup', new LocalStrategy1({ 
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  },
  function (req, username, password, done) {
        process.nextTick(function () {
            var compName = req.body.name;
            // find a user in Mongo with provided username
            User.findOne(username).then(function (user) {
                // already exists
                if (user) {
                    console.log('User already exists with email: ' + username);
                    return done(null, false, req.flash('message', 'User Already Exists'));
                } else {
                    var keygen = require("keygenerator");
                    var apikey = keygen._();
                    // if there is no user with that email
                    // create the user
                    var newUser = User.UserFactory();
                    
                    // set the user's local credentials
                    newUser.companyName = compName;
                    newUser.password = User.createHash(password);
                    newUser.email = username;
                    newUser.apikey = apikey;
                    
                    // save the user
                User.createUser(newUser).then(function (response) {
                        return done(null, newUser);
                    }).fail(function (err) {
                        if (err) {
                            console.log('Error in Saving user: ' + err);
                            return done(null, newUser);
                        }
                    });
                }
            })
      });
    }
));

// API key access to the user
passport.use('localapikey', new LocalStrategy2(
  function (apikey, done) {
        process.nextTick(function () {
            if (apikey !== 'p@yRoLLTri@lApI') {
                User.findByApiKey(apikey).then(function (result) {
                    console.log(result.email._);
                    if (result.email._ !== "") {
                        User.findOne(result.email._).then(function (user) {
                            return done(null, user);
                        });
                    }
                    else
                        return done(null, false, { message: 'Unknown apikey : ' + apikey });
                }).fail(function (err) {
                    if (err) {
                        console.log('Error getting api: ' + err);
                        return done(null, null);
                    }
                });
            }
            else {
                var tempUser = User.UserFactory();
                tempUser.companyName = 'trialuser';
                tempUser.password = 'secrettrial_2017';
                tempUser.email = 'trialuser@payroll.com';
                tempUser.enabled = 'true';

                return done(null, tempUser);
            }
        });
    }
));

// Forgot - Get API key of the user
passport.use('getkey', new LocalStrategy1({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function (req, username, password, done) {
        process.nextTick(function () {
        // check if a user with username exists or not 
        console.log(username);
        console.log(password);          
            User.findOne(username).then(function (user) {
                if (!user) {
                    console.log('User Not Found with username ' + username);
                    return done(null, false, req.flash('message', 'User Not found'));
                }
                // User exists but wrong password, log the error 
                if (!User.isValidPassword(user, password)) {
                    console.log('Invalid Password');
                    return done(null, false, req.flash('message', 'Invalid Password')); // redirect back to login page
                }
                // User and password both match, return user from done method
                // which will be treated like success
                return done(null, user);
            }).fail(function (err) {
                // In case of any error, return using the done method                    
                return done(err);
            });
        });
    }
));

// error handlers
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    req.logout();
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        req.logout();
        res.status(err.status || 500).send(err.message);
        /* res.render('error', {
            message: err.message,
            error: err
        }); */
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    req.logout();
    res.status(err.status || 500).send(err.message);
    /* res.render('error', {
        message: err.message,
        error: {}
    }); */
});

app.get('/', routes.index);
app.get('/trial', routes.trial);
app.post('/register', passport.authenticate('signup', { failureRedirect: '/', failureFlash: true }), routes.register);
app.get('/forgotkey', routes.forgotkey);
app.post('/forgot', passport.authenticate('getkey', { failureRedirect: '/forgotkey', failureFlash: true }), routes.forgot);
app.post('/workevents', passport.authenticate('localapikey', { failureFlash: true }), routes.workevents);

if (!module.parent) {
    http.createServer(app).listen(app.get('port'), function () {
        console.log('Express server listening on port ' + app.get('port'));
    });
}
module.exports = app;