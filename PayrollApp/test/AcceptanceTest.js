﻿var assert = require('assert');
var Browser = require('zombie');
var app = require('../app');

//Home Page - User Registration Test Suite
describe('Test Suite B', function () {
    
    // Test if User registration form exists
    it('should check for user form', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/", function () {
            browser.assert.element('form');
            browser.assert.element('form input[name=name]');
            browser.assert.element('form input[name=email]');
            browser.assert.element('form input[name=password]');
        });
    });
    
    // Test to get API Key (form field values must be changed before running test everytime)
    it('should get a api key', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/", function () {
            browser.fill('name', 'Test Company');
            browser.fill('email', 'testco@gmail.com');
            browser.fill('password', '123456789');
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                assert.equal(browser.text('.panel-heading'), 'Your API Key');
            })
        });
    });
    
    // check-user exists already (form field values must be changed before running test everytime)
    // field values should be same as previous test case
    it('should get a error messsage', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/", function () {
            browser.fill('name', 'Test Company');
            browser.fill('email', 'testco@gmail.com');
            browser.fill('password', '987654321');
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                var errorText = browser.text('.alert.alert-dismissable.alert-danger');
                assert.equal(errorText.substring(errorText.indexOf('User')), 'User Already Exists');
            })
        });
    });
    
    // Test forgot-key link
    it('should redirect to another url', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/", function () {
            browser.clickLink('Forgot API Key?').then(function () {
                assert.ok(browser.success);
                browser.assert.url({ pathname: '/forgotkey' });
            });
        });
    });
});

//Forgot key page Test Suite
describe('Test Suite C', function () {
    
    // Test if Forgot key form exists
    it('should check for forgot-key form', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/forgotkey", function () {
            browser.assert.element('form');
            browser.assert.element('form input[name=email]');
            browser.assert.element('form input[name=password]');
        });
    });
    
    // Test to get registerd API Key (form field values must be changed before running test everytime)
    // should be as test case: 'should get a api key'
    it('should get a registered api key', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/forgotkey", function () {
            browser.fill('email', 'testco@gmail.com');
            browser.fill('password', '123456789');
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                assert.equal(browser.text('.panel-heading'), 'Your registered API Key');
            });
        });
    });
    
    // check-user doesn't exists (form field values must be changed before running test everytime)
    // email field value should be not same as in any of previous test cases
    it('should get a error message for user', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/forgotkey", function () {
            browser.fill('email', 'testco2@gmail.com');
            browser.fill('password', 'qwerty123');
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                var errorText = browser.text('.alert.alert-dismissable.alert-danger');
                assert.equal(errorText.substring(errorText.indexOf('User')), 'User Not found');
            });
        });
    });
    
    // check-user exists, invalid password (form field values must be changed before running test everytime)
    // email field value should be same as in : 'should get a api key' but with differnt password
    it('should get a error message for password', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/forgotkey", function () {
            browser.fill('email', 'testco@gmail.com');
            browser.fill('password', 'qwerty123456');
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                var errorText = browser.text('.alert.alert-dismissable.alert-danger');
                assert.equal(errorText.substring(errorText.indexOf('Invalid')), 'Invalid Password');
            });
        });
    });
    
    // Test user registration link
    it('should redirect to home page', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/forgotkey", function () {
            browser.clickLink("Don't have account? Register here").then(function () {
                assert.ok(browser.success);
                browser.assert.url({ pathname: '/' });
            }).then(done, done);
        });
    });
});

//Test API page Test Suite
describe('Test Suite D', function () {    
    // Test if Trial API form exists
    it('should check for test API form', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/trial", function () {
            browser.assert.element('form');
            browser.assert.attribute('form', 'method', 'post');
            browser.assert.attribute('form', 'id', 'form1');
            browser.assert.element('form textarea[name=jsoninp]');
            browser.assert.element('form input[name=apikey]');
        });
    });
    
    // Test to get work events JSON file for the default JSON input
    it('should get work events: status 200', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/trial", function () {
            browser.pressButton('Generate Work Events').then(function () {
                assert.ok(browser.success);
                assert.equal(browser.response.status, 200);
            });
        });
    });
    
    // Test to get error when wrong JSON configuration is injected
    it('should get a error message for wrong JSON configuration', function () {
        browser = new Browser();
        var textareavalue = [
            {
                "employeeName": "vinod",
                "employeeId": "AD783433",
                "holidays": [
                    "2016-05-25",
                    "2016-08-20",
                    "2016-06-01"
                ],
                "eventsFrom": "2016-01-01",
                "countryCode": "EE"
            }
        ];
        
        browser.visit("http://localhost:3000/trial", function () {
            browser.fill('jsoninp', JSON.stringify(textareavalue));
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                assert.equal(browser.response.status, 200);
                var errorText = browser.text('.alert.alert-dismissable.alert-danger');
                assert.equal(errorText.substring(errorText.indexOf('Bad')), 'Bad Request - Incorrect JSON Configuration');
            });
        });
    });
    
    // Test to get error when empty JSON configuration is injected
    it('should get a error message for empty JSON configuration', function () {
        browser = new Browser();
        browser.visit("http://localhost:3000/trial", function () {
            browser.fill('jsoninp', '[]');
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                assert.equal(browser.response.status, 200);
                var errorText = browser.text('.alert.alert-dismissable.alert-danger');
                assert.equal(errorText.substring(errorText.indexOf('Bad')), 'Bad Request');
            });
        });
    });

    // Test to get error when invalid JSON configuration is injected
    it('should get a error message for invalid JSON configuration', function () {
        browser = new Browser();
        var textareavalue = [
            {
                "employeeName": "vinod",
                "employeeId": "AD783433",
                "holidays": [
                    "2016-05-25",
                    "2016-08-20",
                    "2016-06-01"
                ],
                "eventsFrom": "2016-01-01",
                "eventsTo": "2017-08-17",
                "countryCode": "EE",
            },
        ];
        browser.visit("http://localhost:3000/trial", function () {
            browser.fill('jsoninp', JSON.stringify(textareavalue));
            browser.document.forms[0].submit();
            browser.wait().then(function () {
                assert.ok(browser.success);
                assert.equal(browser.response.status, 500);
                browser.assert.url({ pathname: '/workevents' });
            });
        });
    });
});
