﻿var assert = require('assert');
var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../app');
var expect = chai.expect;
chai.use(chaiHttp);

//Work events Test Suite
describe('Test Suite A', function () {

    // JSON test request for remote API call(with valid API key) - should respond as JSON message
    it("responds with status 200 with JSON", function (done) {
        var testrequest = [
            {
                "employeeName": "vinod",
                "employeeId": "AD783433",
                "holidays": [
                    "2016-05-25",
                    "2016-08-20",
                    "2016-06-01"
                ],
                "eventsFrom": "2016-01-01",
                "eventsTo": "2016-12-08",
                "countryCode": "EE"
            }
        ];
        var testresponse = [
            {
                "employeeName": "vinod",
                "employeeId": "AD783433",
                "workEvents": [
                    {
                        "date": "2016-03-25",
                        "name": "suur reede"
                    },
                    {
                        "date": "2016-03-27",
                        "name": "lihavõtted"
                    },
                    {
                        "date": "2016-05-01",
                        "name": "kevadpüha"
                    },
                    {
                        "date": "2016-05-15",
                        "name": "nelipühade 1. püha"
                    },
                    {
                        "date": "2016-05-25",
                        "name": "Employee Leave"
                    },
                    {
                        "date": "2016-06-01",
                        "name": "Employee Leave"
                    },
                    {
                        "date": "2016-06-23",
                        "name": "võidupüha"
                    },
                    {
                        "date": "2016-06-24",
                        "name": "jaanipäev"
                    },
                    {
                        "date": "2016-08-20",
                        "name": "taasiseseisvumispäev"
                    }
                ]
            }
        ];
        chai.request(app)
            .post('/workevents?apikey=p@yRoLLTri@lApI')
            .send(testrequest)
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(JSON.parse(res.text)).to.deep.equal(testresponse);
                done();
        });
    });
    
    // JSON test request for local API call - should respond with .JSON file
    it("responds with status 200 with JSON file", function (done) {
        var testrequest = [
            {
                "employeeName": "John",
                "employeeId": "KL783433",
                "holidays": [],
                "eventsFrom": "2016-11-28",
                "eventsTo": "2017-08-17",
                "countryCode": "FI"
            }
        ];
        var testresponse = [
            {
                "employeeName": "John",
                "employeeId": "KL783433",
                "workEvents": [
                    {
                        "date": "2016-12-06",
                        "name": "Itsenäisyyspäivä"
                    },
                    {
                        "date": "2016-12-24",
                        "name": "Jouluaatto"
                    },
                    {
                        "date": "2016-12-25",
                        "name": "Joulupäivä"
                    },
                    {
                        "date": "2016-12-26",
                        "name": "2. joulupäivä"
                    },
                    {
                        "date": "2016-12-31",
                        "name": "Uudenvuodenaatto"
                    },
                    {
                        "date": "2017-01-01",
                        "name": "Uudenvuodenpäivä"
                    },
                    {
                        "date": "2017-01-06",
                        "name": "Loppiainen"
                    },
                    {
                        "date": "2017-04-14",
                        "name": "Pitkäperjantai"
                    },
                    {
                        "date": "2017-04-16",
                        "name": "Pääsiäispäivä"
                    },
                    {
                        "date": "2017-04-17",
                        "name": "2. pääsiäispäivä"
                    },
                    {
                        "date": "2017-05-01",
                        "name": "Vappu"
                    },
                    {
                        "date": "2017-05-25",
                        "name": "Helatorstai"
                    },
                    {
                        "date": "2017-06-04",
                        "name": "Helluntaipäivä"
                    },
                    {
                        "date": "2017-06-23",
                        "name": "Juhannusaatto"
                    },
                    {
                        "date": "2017-06-24",
                        "name": "Juhannuspäivä"
                    }
                ]
            }
        ];
        chai.request(app)
            .post('/workevents?apikey=p@yRoLLTri@lApI')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({ jsoninp: JSON.stringify(testrequest) })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(res).to.have.header('content-disposition', 'attachment; filename=workevents.json');
                expect(JSON.parse(res.text)).to.deep.equal(testresponse);
                done();
        });
    });
    
    // Remote/local API call (valid API key) without JSON input
    it("responds with status 400/Bad Request", function (done) {
        chai.request(app)
            .post('/workevents?apikey=p@yRoLLTri@lApI')
            .end(function (err, res) {
                expect(res).to.have.status(400);
                expect(res.text).to.equal('Bad Request');
                done();
        });

    });
    
    // Remote/local API call (valid API key) with invalid JSON configuration - all objects are mandatory excepts 'holidays'
    it("responds with status 400/Bad Request:Incorrect JSON Configuration", function (done) {
        var testrequest = [
            {
                "employeeName": "vinod",
                "employeeId": "AD783433",
                "holidays": [
                    "2016-05-25",
                    "2016-08-20",
                    "2016-06-01"
                ],
                "eventsFrom": "2016-01-01",
                "countryCode": "EE"
            }
        ];
        chai.request(app)
            .post('/workevents?apikey=p@yRoLLTri@lApI')
            .send(testrequest)
            .end(function (err, res) {
                expect(res).to.have.status(400);
                expect(res.text).to.equal('Bad Request - Incorrect JSON Configuration');
                done();
        });

    });
    
    //Remote/local API call - Invalid API Key
    it("responds with status 401/Unauthorized", function (done) {
        chai.request(app)
            .post('/workevents?apikey=sdp9fuhsfvbrfgdsufhaspdkfahsvbui')
            .end(function (err, res) {
                expect(res).to.have.status(401);
                expect(res.text).to.equal('Unauthorized');
                done();
        });

    });
    
    //Remote/local API call - without API key
    it("responds with status 401/Unauthorized-without apikey", function (done) {
        var testrequest = [
            {
                "employeeName": "vinod",
                "employeeId": "AD783433",
                "holidays": [
                    "2016-05-25",
                    "2016-08-20",
                    "2016-06-01"
                ],
                "eventsFrom": "2016-01-01",
                "eventsTo": "2016-12-08",
                "countryCode": "EE"
            }
        ];
        chai.request(app)
            .post('/workevents')
            .send(testrequest)
            .end(function (err, res) {
                expect(res).to.have.status(401);
                expect(res.text).to.equal('Unauthorized');
                done();
        });

    }); 

});
