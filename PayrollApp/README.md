﻿# PayRollApp - Configurator coding task

A payroll app developed using Node.JS and Visual Studio 2015. 

# Functions

  - To read aJSON configuration from a REST API, This app has an interface to test the REST API by giving a JSON as a form input.
  - The app has itself a package that reads the bank holidays.
  - It generates the output as a '.JSON' file  and downloads automatically from the browser.
  - The web API supports multiple countries work events generation.
  - The API can even push work events to remotely securely by using API authentication.

## Security
 The app supports can able to push the work events to a remote service as JSON.  The security is more important when transferring the data to the remote service. All the remote requests and responses are handled securely using 'API Key Authentication' service. This service is implemented by using various strategies and algorithms.
 
 To get a API key, the user needs to be registered. After the registration, the app automatically gives access to the remote services. There is also a service to retrieve a forgotten API Key. All the security errors and exceptions are handled. The app uses 'Azure Storage' as a database to store User credintials.

# JSON Configuration
Here is the JSON configuration/format which the app accepts and responds:

## Request (application/json)
List of employees of various countries can be processed at a time. All the elements in this JSON configuration are mandatory except 'holidays'.
'holidays' is optional because employee may not have finalized his/her forecasted holidays.
```sh
POST /workevents?apikey=

 {
 	"employeeName": "",
 	"employeeId": "",
 	"holidays": [],
 	"eventsFrom": "YYYY-MM-DD",
 	"eventsTo": "YYYY-MM-DD",
 	"countryCode": "FI"
 }	
]
```
The 'apikey' in the request URL has to be user's registered API key. The country code must be strictly followed.

```sh
Supported Countries:
├── AO: Angola
├── AM: Հայաստան
├── AR: Argentina
├── BG: България
├── BI: République du Burundi
├── BO: Bolivia
├── BR: Brasil
├── BS: Bahamas
├── BW: Botswana
├── BZ: Belize
├── CD: République démocratique du Congo
├── CF: République centrafricaine
├── CG: République du Congo
├── CM: Cameroun
├── CO: Colombia
├── CR: Costa Rica
├── CU: Cuba
├── CY: Κύπρος
├── CZ: Česká republika
├── DK: Danmark
├── DO: República Dominicana
├── EE: Eesti
├── ET: ኢትዮጵያ
├── FI: Suomi
├── GA: Gabon
├── GD: Grenada
├── GR: Ελλάδα
├── GQ: República de Guinea Ecuatorial
├── GT: Guatemala
├── GU: Guam
├── HN: Honduras
├── HT: Haïti
├── HU: Magyarország
├── IE: Ireland
├── IS: Ísland
├── JM: Jamaica
├── JP: 日本
├── KE: KE
├── LI: Lichtenstein
├── LT: Lietuva
├── LU: Luxembourg
├── LV: Latvija
├── MC: Monaco
├── ME: Crna Gora
├── MG: Repoblikan'i Madagasikara
├── MX: México
├── MT: Malta
├── MZ: Moçambique
├── MW: Malawi
├── NA: Namibia
├── NI: Nicaragua
├── NL: Nederland
├── NO: Norge
├── PA: Panamá
├── PL: Polska
├── PT: Portugal
├── PY: Paraguay
├── RO: Romania
├── RU: Россия
├── RW: Repubulika y'u Rwanda
├── SE: Sverige
├── SS: South Sudan
├── TG: République togolaise
├── TR: Türkiye
├── TZ: Tanzania
├── UG: Uganda
├── UY: Uruguay
├── ZA: South Africa
├── ZM: Zambia
└── ZW: Zimbabwe
```
A sample valid JSON request is: 
```
[
 {
 	"employeeName": "vinod",
 	"employeeId": "AD783433",
 	"holidays": [
      "2016-05-25",
      "2016-08-20",
      "2016-06-01"
    ],
 	"eventsFrom": "2016-01-01",
 	"eventsTo": "2016-12-08",
 	"countryCode": "EE"
 },
 {
 	"employeeName": "John",
 	"employeeId": "KL783433",
 	"holidays": [],
 	"eventsFrom": "2016-11-28",
 	"eventsTo": "2017-08-17",
 	"countryCode": "FI"
 }	
]
```
## Response (application/json)
If its an internal call to the app, then  the app will respond with a '.JSON' file. If its a valid remote call, then it will be given JSON reponse. There are various HTTP Status Codes are used for the response. The main codes 200, 401, 404 and 500.

## Testing
The app has Unit and Acceptance cases written under 'test' folder. The test cases are written by using  Mocha Test Framework along with Chai.js and Zombie.js for Unit and Acceptance tests repectively.
Note: To make acceptance tests pass every time, the instructions specified in 'AcceptanceTest.js' should be strictly followed.
Each test case has different instructions.

## Deployed to Cloud
You can use the app which is running already [here]: <http://payrollappcloudator.azurewebsites.net/>

